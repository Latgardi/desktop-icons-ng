# This file is distributed under the same license as the PACKAGE package.
# Jordi Mas i Hernandez <jmas@softcatala.org>, 2019-2021
#
msgid ""
msgstr ""
"Project-Id-Version: 1.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-06-18 22:06+0200\n"
"PO-Revision-Date: 2019-07-22 10:24+0200\n"
"Last-Translator: Jordi Mas <jmas@softcatala.org>\n"
"Language-Team: Catalan <info@softcatala.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Poedit 2.2.1\n"

#: askConfirmPopup.js:36 askNamePopup.js:37 desktopIconsUtil.js:211
#: desktopManager.js:1481
msgid "Cancel"
msgstr "Cancel·la"

#: askConfirmPopup.js:37
msgid "Delete"
msgstr "Suprimeix"

#: askNamePopup.js:36
msgid "OK"
msgstr "D'acord"

#: askRenamePopup.js:40
msgid "Folder name"
msgstr "Nom de la carpeta"

#: askRenamePopup.js:40
msgid "File name"
msgstr "Nom del fitxer"

#: askRenamePopup.js:47
msgid "Rename"
msgstr "Canvia el nom"

#: desktopIconsUtil.js:80
msgid "Command not found"
msgstr "No s'ha trobat l'ordre"

#: desktopIconsUtil.js:202
msgid "Do you want to run “{0}”, or display its contents?"
msgstr "Voleu executar «{0}» o mostrar el seu contingut?"

#: desktopIconsUtil.js:203
msgid "“{0}” is an executable text file."
msgstr "«{0}» és un fitxer de text executable."

#: desktopIconsUtil.js:207
msgid "Execute in a terminal"
msgstr "Obre al Terminal"

#: desktopIconsUtil.js:209
msgid "Show"
msgstr "Mostra"

#: desktopIconsUtil.js:213
msgid "Execute"
msgstr "Executa"

#: desktopManager.js:135
msgid "Nautilus File Manager not found"
msgstr "No s'ha trobat el gestor de fitxers del Nautilus"

#: desktopManager.js:136
msgid "The Nautilus File Manager is mandatory to work with Desktop Icons NG."
msgstr ""
"El gestor de fitxers Nautilus és obligatori per a treballar amb les icones "
"de l'escriptori NG."

#: desktopManager.js:563
msgid "New Folder"
msgstr "Carpeta nova"

#: desktopManager.js:567
msgid "New Document"
msgstr "Document nou"

#: desktopManager.js:572
msgid "Paste"
msgstr "Enganxa"

#: desktopManager.js:576
msgid "Undo"
msgstr "Desfés"

#: desktopManager.js:580
msgid "Redo"
msgstr "Refés"

#: desktopManager.js:586
msgid "Select all"
msgstr "Selecciona-ho tot"

#: desktopManager.js:592
msgid "Show Desktop in Files"
msgstr "Mostra l'escriptori al Fitxers"

#: desktopManager.js:596 fileItem.js:913
msgid "Open in Terminal"
msgstr "Obre al Terminal"

#: desktopManager.js:602
msgid "Change Background…"
msgstr "Canvia el fons de l'escriptori…"

#: desktopManager.js:611
msgid "Display Settings"
msgstr "Paràmetres de la pantalla"

#: desktopManager.js:618
msgid "Desktop Icons settings"
msgstr "Paràmetres de les icones de l'escriptori"

#: desktopManager.js:629
msgid "Scripts"
msgstr "Scripts"

#: desktopManager.js:1148 desktopManager.js:1196
msgid "Error while deleting files"
msgstr "S'ha produït un error en suprimir els fitxers"

#: desktopManager.js:1222
msgid "Are you sure you want to permanently delete these items?"
msgstr "Esteu segur que voleu suprimir permanentment aquests elements?"

#: desktopManager.js:1223
msgid "If you delete an item, it will be permanently lost."
msgstr "Si suprimiu un element, es perdrà permanentment."

#: desktopManager.js:1339
msgid "New folder"
msgstr "Carpeta nova"

#: desktopManager.js:1414
msgid "Can not email a Directory"
msgstr "No es pot enviar un directori per correu electrònic"

#: desktopManager.js:1415
msgid "Selection includes a Directory, compress the directory to a file first."
msgstr ""
"La selecció inclou un directori, comprimeix-hi el directori en un fitxer."

#: desktopManager.js:1479
msgid "Select Extract Destination"
msgstr ""

#: desktopManager.js:1482
#, fuzzy
msgid "Select"
msgstr "Selecciona-ho tot"

#. TRANSLATORS: "Home" is the text that will be shown in the user's personal folder
#: fileItem.js:153
msgid "Home"
msgstr "Inici"

#: fileItem.js:809
msgid "Open All..."
msgstr "Obre-ho tot..."

#: fileItem.js:809
msgid "Open"
msgstr "Obre"

#: fileItem.js:816
msgid "Open All With Other Application..."
msgstr "Obre amb una altra aplicació..."

#: fileItem.js:816
msgid "Open With Other Application"
msgstr "Obre amb una altra aplicació..."

#: fileItem.js:820
msgid "Launch using Dedicated Graphics Card"
msgstr "Llança usant targeta gràfica dedicada"

#: fileItem.js:828
msgid "Cut"
msgstr "Retalla"

#: fileItem.js:831
msgid "Copy"
msgstr "Copia"

#: fileItem.js:835
msgid "Rename…"
msgstr "Canvia el nom…"

#: fileItem.js:839
msgid "Move to Trash"
msgstr "Mou a la paperera"

#: fileItem.js:843
msgid "Delete permanently"
msgstr "Suprimeix permanentment"

#: fileItem.js:849
msgid "Don't Allow Launching"
msgstr "No permetis que s'iniciï"

#: fileItem.js:849
msgid "Allow Launching"
msgstr "Permet que s'iniciï"

#: fileItem.js:856
msgid "Empty Trash"
msgstr "Buida la paperera"

#: fileItem.js:863
msgid "Eject"
msgstr "Expulsa"

#: fileItem.js:870
msgid "Unmount"
msgstr "Desmunta"

#: fileItem.js:885
msgid "Extract Here"
msgstr ""

#: fileItem.js:888
msgid "Extract To..."
msgstr ""

#: fileItem.js:893
msgid "Send to..."
msgstr "Envia a..."

#: fileItem.js:897
msgid "Compress {0} file"
msgid_plural "Compress {0} files"
msgstr[0] "Comprimeix {0} fitxer"
msgstr[1] "Comprimeix {0} fitxers"

#: fileItem.js:900
msgid "New Folder with {0} item"
msgid_plural "New Folder with {0} items"
msgstr[0] "Carpeta nova amb {0} element"
msgstr[1] "Carpeta nova amb {0} elements"

#: fileItem.js:905
msgid "Common Properties"
msgstr "Propietats comunes"

#: fileItem.js:905
msgid "Properties"
msgstr "Propietats"

#: fileItem.js:909
msgid "Show All in Files"
msgstr "Mostra al Fitxers"

#: fileItem.js:909
msgid "Show in Files"
msgstr "Mostra al Fitxers"

#: preferences.js:91
msgid "Settings"
msgstr "Paràmetres"

#: preferences.js:98
msgid "Size for the desktop icons"
msgstr "Mida de les icones d'escriptori"

#: preferences.js:98
msgid "Tiny"
msgstr "Diminuta"

#: preferences.js:98
msgid "Small"
msgstr "Petita"

#: preferences.js:98
msgid "Standard"
msgstr "Estàndard"

#: preferences.js:98
msgid "Large"
msgstr "Gran"

#: preferences.js:99
msgid "Show the personal folder in the desktop"
msgstr "Mostra la carpeta personal a l'escriptori"

#: preferences.js:100
msgid "Show the trash icon in the desktop"
msgstr "Mostra la icona de la paperera a l'escriptori"

#: preferences.js:101 schemas/org.gnome.shell.extensions.ding.gschema.xml:38
msgid "Show external drives in the desktop"
msgstr "Mostra les unitats externes a l'escriptori"

#: preferences.js:102 schemas/org.gnome.shell.extensions.ding.gschema.xml:43
msgid "Show network drives in the desktop"
msgstr "Mostra les unitats de xarxa a l'escriptori"

#: preferences.js:105
msgid "New icons alignment"
msgstr "Alineació de les icones noves"

#: preferences.js:106
msgid "Top-left corner"
msgstr "Cantonada superior esquerra"

#: preferences.js:107
msgid "Top-right corner"
msgstr "Cantonada superior dreta"

#: preferences.js:108
msgid "Bottom-left corner"
msgstr "Cantonada inferior esquerra"

#: preferences.js:109
msgid "Bottom-right corner"
msgstr "Cantonada inferior dreta"

#: preferences.js:111 schemas/org.gnome.shell.extensions.ding.gschema.xml:48
msgid "Add new drives to the opposite side of the screen"
msgstr "Afegeix noves unitats al costat oposat de la pantalla"

#: preferences.js:112
msgid "Highlight the drop place during Drag'n'Drop"
msgstr "Ressalta el lloc desplegable durant l'arrossegat"

#: preferences.js:116
msgid "Settings shared with Nautilus"
msgstr "Paràmetres compartits amb el Nautilus"

#: preferences.js:122
msgid "Click type for open files"
msgstr "Feu clic al tipus per a obrir fitxers"

#: preferences.js:122
msgid "Single click"
msgstr "Un sol clic"

#: preferences.js:122
msgid "Double click"
msgstr "Doble clic"

#: preferences.js:123
msgid "Show hidden files"
msgstr "Mostra els fitxers ocults"

#: preferences.js:124
msgid "Show a context menu item to delete permanently"
msgstr "Mostra un element de menú contextual per a suprimir-lo permanentment"

#: preferences.js:129
msgid "Action to do when launching a program from the desktop"
msgstr "Acció a fer quan s'iniciï un programa des de l'escriptori"

#: preferences.js:130
msgid "Display the content of the file"
msgstr "Mostra el contingut del fitxer"

#: preferences.js:131
msgid "Launch the file"
msgstr "Executa el fitxer"

#: preferences.js:132
msgid "Ask what to do"
msgstr "Preguntar què fer"

#: preferences.js:138
msgid "Show image thumbnails"
msgstr "Mostra les miniatures de les imatges"

#: preferences.js:139
msgid "Never"
msgstr "Mai"

#: preferences.js:140
msgid "Local files only"
msgstr "Només fitxers locals"

#: preferences.js:141
msgid "Always"
msgstr "Sempre"

#: prefs.js:37
msgid ""
"To configure Desktop Icons NG, do right-click in the desktop and choose the "
"last item: 'Desktop Icons settings'"
msgstr ""
"Per a configurar les icones de l'escriptori NG, feu clic dret a l'escriptori "
"i trieu l'últim element: «Paràmetres de les icones de l'escriptori»"

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:18
msgid "Icon size"
msgstr "Mida d'icona"

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:19
msgid "Set the size for the desktop icons."
msgstr "Estableix la mida per les icones de l'escriptori."

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:23
msgid "Show personal folder"
msgstr "Mostra la carpeta personal"

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:24
msgid "Show the personal folder in the desktop."
msgstr "Mostra la carpeta personal a l'escriptori."

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:28
msgid "Show trash icon"
msgstr "Mostra la icona de la paperera"

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:29
msgid "Show the trash icon in the desktop."
msgstr "Mostra la icona de la paperera a l'escriptori."

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:33
msgid "New icons start corner"
msgstr "Angle d'inici de les icones noves"

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:34
msgid "Set the corner from where the icons will start to be placed."
msgstr "Estableix la cantonada des d'on s'iniciaran les icones."

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:39
msgid "Show the disk drives connected to the computer."
msgstr "Mostra les unitats de disc connectades a l'ordinador."

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:44
msgid "Show mounted network volumes in the desktop."
msgstr "Mostra els volums de xarxa muntats a l'escriptori."

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:49
msgid ""
"When adding drives and volumes to the desktop, add them to the opposite side "
"of the screen."
msgstr ""
"Quan s'afegeixin unitats i volums a l'escriptori, afegiu-los a la part "
"oposada de la pantalla."

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:53
msgid "Shows a rectangle in the destination place during DnD"
msgstr "Mostra un rectangle al lloc de destinació durant el DnD"

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:54
msgid ""
"When doing a Drag'n'Drop operation, marks the place in the grid where the "
"icon will be put with a semitransparent rectangle."
msgstr ""
"En fer una operació d'arrossegar i deixar anar, marca el lloc a la graella "
"on la icona es posarà amb un rectangle semitransparent."
